package com.linemoney.cassandracenter.test;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import com.linemoney.cassandracenter.helper.LmCassandraCenterConstants;
import com.linemoney.cassandracenter.model.ClearingView;
import com.linemoney.cassandracenter.service.client.SimpleClient;
import com.linemoney.cassandracenter.service.jdbc.CassandraDAO;
import com.linemoney.cassandracenter.service.jdbc.ConnectionFactory;
@Ignore
public class SimpleClientTest {

	@Test
	public void testClient() {
		
		
		SimpleClient client = SimpleClient.getInstance();
		ConnectionFactory pool = ConnectionFactory.getInstance();
		CassandraDAO viewDao = new CassandraDAO(pool, "linemoney_portal.lm_cassandra_view");
	    client.connect();
	    
	    
	    List<ClearingView> views = new ArrayList<ClearingView>();
		try {
			views = viewDao.selectAll();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
		client.querySchema("linemoney_portal","cassandra_view");
		
		client.close();
	    
	}
	


}
