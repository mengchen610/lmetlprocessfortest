package com.linemoney.cassandracenter.helper;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.json.JSONException;
import com.linemoney.cassandracenter.model.*;
import com.linemoney.util.security.EncryptionUtil;

public class ConvertHelper {
	
	private CassandraJsonUtil helper;
	private static final Logger logger = Logger.getLogger(ConvertHelper.class);
	
	public ConvertHelper(){
		   helper = new CassandraJsonUtil();
	}
       
	public List<Transaction> parseTransaction(ResultSet rs)
			throws JSONException {
		List<Transaction> transactions = new ArrayList<Transaction>();
		try {
			while (rs.next()) {
				try {
					String transactionId = rs.getString("transactionId");
					String taskId = rs.getString("taskId");
					String statusCd = rs.getString("statusCd");
					Timestamp addTime = rs.getTimestamp("addTime");
					Transaction transaction = new Transaction(transactionId,
							taskId, statusCd, addTime.getTime());
					transactions.add(transaction);
				} catch (SQLException e) {
					logger.error("Transaction fetch fail");
					logger.error(e.getMessage());
				}
			}
		} catch (SQLException e) {
			logger.error("Transaction fetch fail");
			logger.error(e.getMessage());
		}
		return transactions;
	}

	public List<Task> parseTask(ResultSet rs) throws JSONException {
		List<Task> tasks = new ArrayList<Task>();
		try {
			while (rs.next()) {
				try {
					String taskId = rs.getString("taskId");
					String orderId = rs.getString("orderId");
					String fromAccountId = rs.getString("fromAccountId");
					String fromAccountDetail = rs
							.getString("fromAccountDetail");
					String toAccountId = rs.getString("toAccountId");
					String toAccountDetail = rs.getString("toAccountDetail");
					double fromAmount = rs.getDouble("fromAmount");
					String statusCd = rs.getString("statusCd");
					String transactionTypeCd = rs
							.getString("transactionTypeCd");
					int taskIndex = rs.getInt("taskIndex");
					Timestamp addTime = rs.getTimestamp("addTime");
					Timestamp updateTime = rs.getTimestamp("updateTime");
					double serviceFee = rs.getDouble("serviceFee");
					String obj = helper.getTaskJSONObject(fromAccountId,
							fromAccountDetail, toAccountId, toAccountDetail,
							fromAmount, statusCd, transactionTypeCd, taskIndex,
							addTime, updateTime, serviceFee);
					String encryption = EncryptionUtil.encrypt(obj);
					Task task = new Task(taskId, orderId, encryption);
					tasks.add(task);
				} catch (SQLException e) {
					logger.error("Task fetch fail");
					logger.error(e.getMessage());
				}
			}
		} catch (SQLException e) {
			logger.error("Task fetch fail");
			logger.error(e.getMessage());
		}
		return tasks;

	}

	public List<Order> parseOrder(ResultSet rs) throws JSONException{
		     List<Order> orders = new ArrayList<Order>();
		   
		   try {
			while (rs.next()){
				   
				   try {
					
					String orderId = rs.getString("orderId");
					String originInstance = rs.getString("originInstance");
					long userId = rs.getLong("userId");
					String parentOrderId = rs.getString("parentOrderId");
					String fromCurrencyCd = rs.getString("fromCurrencyCd");
					String toCurrencyCd = rs.getString("toCurrencyCd");
					String fromAccountId = rs.getString("fromAccountId");
					String toAccountId = rs.getString("toAccountId");
					String fromCountryCd = rs.getString("fromCountryCd");
					String toCountryCd = rs.getString("toCountryCd");
					double fromAmount = rs.getDouble("fromAmount");
					String fxTypeCd = rs.getString("fxTypeCd");
					double fxRate = rs.getDouble("fxRate");
					double baseRate = rs.getDouble("baseRate");
					String serviceTypeCd = rs.getString("serviceTypeCd");
					double serviceFee = rs.getDouble("serviceFee");
					String recurringTypeCd = rs.getString("recurringTypeCd");
				    Timestamp orderTime = rs.getTimestamp("orderTime");	
				    String durationTypeCd = rs.getString("durationTypeCd");
				    String processStatusCd = rs.getString("processStatusCd");
				    Timestamp processTime = rs.getTimestamp("processTime");
				    String purpose = rs.getString("purpose");
				    String active_flag = rs.getString("activeFlag");
				    String addUser = rs.getString("addUser");
				    Timestamp addTime = rs.getTimestamp("addTime");
				    String updateUser = rs.getString("updateUser");
				    Timestamp updateTime = rs.getTimestamp("updateTime");
				    String userId_uuid = rs.getString("userId_uuid");
				  
				    String obj = helper.getOrderJSONObject(orderId, userId, originInstance, parentOrderId, fromCurrencyCd, 
				    		toCurrencyCd, fromAccountId, toAccountId, fromCountryCd, toCountryCd, fromAmount, fxTypeCd, fxRate,
				    		baseRate, serviceTypeCd, serviceFee, recurringTypeCd, orderTime, durationTypeCd, 
				    		processStatusCd, processTime, purpose, active_flag, addUser, addTime, updateUser, updateTime,userId_uuid);
					   
			        String encryption = EncryptionUtil.encrypt(obj);
				    if (orderTime == null || updateTime == null){
				    	logger.info("No value for time for order:" + orderId);
				    } else {
				    	Order order = new Order(orderId, userId_uuid, orderTime.getTime(), encryption, updateTime.getTime(), processStatusCd);
				    	orders.add(order);
				    }
				    
				} catch (SQLException e) {
					logger.error("order fetch fail");
					logger.error(e.getMessage());
				}
				   
			   }
		} catch (SQLException e) {
			logger.error("order fetch fail");
			logger.error(e.getMessage());
		}
		   return orders;
	}
	
	public List<User> parseUser(ResultSet rs) throws JSONException{
		 
		 List<User> users = new ArrayList<User>();
		 
		 try {
				while (rs.next()){
					  
					   try {
						
						String userId_uuid = rs.getString("uuid_");
						long userId = rs.getLong("userId");
						long companyId = rs.getLong("companyId");
						Timestamp createDate = rs.getTimestamp("createDate");
						Timestamp modifiedDate = rs.getTimestamp("modifiedDate");
						int defaultUser = rs.getInt("defaultUser");
						long contactId = rs.getLong("contactId");
						String screenName = rs.getString("screenName");
						String emailAddress = rs.getString("emailAddress");
						long facebookId = rs.getLong("facebookId");
						String languageId = rs.getString("languageId");
						String timeZoneId = rs.getString("timeZoneId");
						String comments = rs.getString("comments");
						String firstName = rs.getString("firstName");
						String middleName = rs.getString("middleName");
						String lastName = rs.getString("lastName");
						String jobTitle = rs.getString("jobTitle");
						int lockout = rs.getInt("lockout");
						Timestamp lockoutDate = rs.getTimestamp("lockoutDate");
						int agreedToTermsOfUser = rs.getInt("agreedToTermsOfUse");
						int emailAddressVerified = rs.getInt("emailAddressVerified");
						int status = rs.getInt("status");  
					  
					    String obj = helper.getUserJSONObject(userId_uuid, userId, companyId, createDate, modifiedDate, defaultUser, contactId, screenName, emailAddress, facebookId, languageId, timeZoneId, comments, firstName, middleName, lastName, jobTitle, lockout, lockoutDate, agreedToTermsOfUser, emailAddressVerified, status);
					    String encryption = EncryptionUtil.encrypt(obj);
					
						User user = new User(userId_uuid, encryption, languageId);
						users.add(user);
					} catch (SQLException e) {
						logger.error("User fetch fail");
						logger.error(e.getMessage());
					}
					   
				   }
			} catch (SQLException e) {
				logger.error("User fetch fail");
				logger.error(e.getMessage());
			}
			   return users;
		 
	}
	
	public List<Job> parseJob(ResultSet rs) throws JSONException{
		   List<Job> jobs = new ArrayList<Job>();
		   
		   try {
				while (rs.next()) {
					try {
						String jobId = rs.getString("jobId");
						String statusCd = rs.getString("statusCd");
						Timestamp addTime = rs.getTimestamp("addTime");
						Timestamp updateTime = rs.getTimestamp("updateTime");
						Job job = new Job(jobId, statusCd, addTime.getTime(), updateTime.getTime());
						jobs.add(job);
						
					} catch (SQLException e) {
						logger.info("Job fetch fail");
						logger.info(e.getMessage());
					}
				}
			} catch (SQLException e) {
				logger.error("Job fetch fail");
				logger.error(e.getMessage());
			} 
		    return jobs;
	}
	
	public List<Account> parseAccount(ResultSet rs) throws JSONException {
		List<Account> accounts = new ArrayList<Account>();
		try {
			while (rs.next()) {
				try {
					String accountId = rs.getString("accountId");
					long userId = rs.getLong("userId");
					String accountHolderId = rs.getString("accountHolderId");
					long bankId = rs.getLong("bankId");
					String accountTypeCd = rs.getString("accountTypeCd");
					String bankName = rs.getString("bankName");
					String accountName = rs.getString("accountName");
					String detailJSON = rs.getString("detailJSON");
					String identity = rs.getString("identity");
					String currencyCd = rs.getString("currencyCd");
					double balance = rs.getDouble("balance");
					String paymentType = rs.getString("paymentType");
					String password_ = rs.getString("password_");
					Timestamp expirationDate = rs
							.getTimestamp("expirationDate");
					String activeFlag = rs.getString("activeFlag");
					String addUser = rs.getString("addUser");
					Timestamp addTime = rs.getTimestamp("addTime");
					String updateUser = rs.getString("updateUser");
					Timestamp updateTime = rs.getTimestamp("updateTime");

					String userId_uuid = rs.getString("userId_uuid");

					String accountHolder_accountHolderId = rs
							.getString("accountHolder_accountHolderId");
					long accountHolder_userId = rs
							.getLong("accountHolder_userId");
					String accountHolder_fullName = rs
							.getString("accountHolder_fullName");
					String accountHolder_lastName = rs
							.getString("accountHolder_lastName");
					String accountHolder_firstName = rs
							.getString("accountHolder_firstName");
					String accountHolder_middleName = rs
							.getString("accountHolder_middleName");
					String accountHolder_email = rs
							.getString("accountHolder_email");
					String accountHolder_addressLine1 = rs
							.getString("accountHolder_addressLine1");
					String accountHolder_addressLine2 = rs
							.getString("accountHolder_addressLine2");
					String accountHolder_addressLine3 = rs
							.getString("accountHolder_addressLine3");
					String accountHolder_city = rs
							.getString("accountHolder_city");
					String accountHolder_state_ = rs
							.getString("accountHolder_state_");
					String accountHolder_zipCode = rs
							.getString("accountHolder_zipCode");
					String accountHolder_countryCd = rs
							.getString("accountHolder_countryCd");
					String accountHolder_phone = rs
							.getString("accountHolder_phone");
					String accountHolder_identityNum = rs
							.getString("accountHolder_identityNum");
					String accountHolder_activeFlag = rs
							.getString("accountHolder_activeFlag");
					String accountHolder_addUser = rs
							.getString("accountHolder_addUser");
					Timestamp accountHolder_addTime = rs
							.getTimestamp("accountHolder_addTime");
					String accountHolder_updateUser = rs
							.getString("accountHolder_updateUser");
					Timestamp accountHolder_updateTime = rs
							.getTimestamp("accountHolder_updateTime");

					long bank_bankId = rs.getLong("bank_bankId");
					String bank_bankCd = rs.getString("bank_bankCd");
					String bank_languageCd = rs.getString("bank_languageCd");
					String bank_bankTypeCd = rs.getString("bank_bankTypeCd");
					String bank_bankName = rs.getString("bank_bankName");
					String bank_description = rs.getString("bank_description");
					String bank_bankSwift = rs.getString("bank_bankSwift");
					String bank_bankRouting = rs.getString("bank_bankRouting");
					String bank_addressLine1 = rs
							.getString("bank_addressLine1");
					String bank_addressLine2 = rs
							.getString("bank_addressLine2");
					String bank_addressLine3 = rs
							.getString("bank_addressLine3");
					String bank_countryCd = rs.getString("bank_countryCd");
					String bank_stateCd = rs.getString("bank_stateCd");
					String bank_city = rs.getString("bank_city");
					String bank_postCode = rs.getString("bank_postCode");
					String bank_phone = rs.getString("bank_phone");
					String bank_activeFlag = rs.getString("bank_activeFlag");
					String bank_addUser = rs.getString("bank_addUser");
					Timestamp bank_addTime = rs.getTimestamp("bank_addTime");
					String bank_updateUser = rs.getString("bank_updateUser");
					Timestamp bank_updateTime = rs
							.getTimestamp("bank_updateTime");
					String bank_currencyCd = rs.getString("bank_currencyCd");
					long bank_payMethodId = rs.getLong("bank_payMethodId");

					int payMethod_payMethodId = rs
							.getInt("payMethod_payMethodId");
					String payMethod_payMethodName = rs
							.getString("payMethod_payMethodName");
					String payMethod_serviceName = rs
							.getString("payMethod_serviceName");
					String payMethod_currencyCd = rs
							.getString("payMethod_currencyCd");
					String payMethod_countryCd = rs
							.getString("payMethod_countryCd");
					String payMethod_identity = rs
							.getString("payMethod_identity");
					String payMethod_description = rs
							.getString("payMethod_description");
					Timestamp payMethod_updateTime = rs
							.getTimestamp("payMethod_updateTime");
					String account_obj = helper.getAccountJSONObject(accountId,
							userId, accountHolderId, bankId, accountTypeCd,
							bankName, accountName, detailJSON, identity,
							currencyCd, balance, paymentType, password_,
							expirationDate, activeFlag, addUser, addTime,
							updateUser, updateTime);
					String holder_obj = helper.getAccountHolderJSONObject(
							accountHolder_accountHolderId,
							accountHolder_userId, accountHolder_fullName,
							accountHolder_lastName, accountHolder_firstName,
							accountHolder_middleName, accountHolder_email,
							accountHolder_addressLine1,
							accountHolder_addressLine2,
							accountHolder_addressLine3, accountHolder_city,
							accountHolder_state_, accountHolder_zipCode,
							accountHolder_countryCd, accountHolder_phone,
							accountHolder_identityNum,
							accountHolder_activeFlag, accountHolder_addUser,
							accountHolder_addTime, accountHolder_updateUser,
							accountHolder_updateTime);
					String payMethod_obj = helper.getPaymentMethodJSONObject(
							payMethod_payMethodId, payMethod_payMethodName,
							payMethod_serviceName, payMethod_currencyCd,
							payMethod_countryCd, payMethod_identity,
							payMethod_description, payMethod_updateTime);
					// double check whether can directly put as string not as
					// jsonobject
					// for payMethod_obj to bank_obj
					String bank_obj = helper.getBankJSONObject(bank_bankId,
							bank_bankCd, bank_languageCd, bank_bankTypeCd,
							bank_bankName, bank_description, bank_bankSwift,
							bank_bankRouting, bank_addressLine1,
							bank_addressLine2, bank_addressLine3,
							bank_countryCd, bank_stateCd, bank_city,
							bank_postCode, bank_phone, bank_activeFlag,
							bank_addUser, bank_addTime, bank_updateUser,
							bank_updateTime, bank_currencyCd, payMethod_obj);
					
					String account_encryption = EncryptionUtil
							.encrypt(account_obj);
					String holder_encryption = EncryptionUtil
							.encrypt(holder_obj);
					String bank_encryption = EncryptionUtil.encrypt(bank_obj);
					Account account = new Account(accountId, userId_uuid,
							holder_encryption, bank_encryption, accountTypeCd, account_encryption);
					accounts.add(account);
				} catch (SQLException e) {
					logger.error("Account fetch fail");
					logger.error(e.getMessage());
				}
			}
		} catch (SQLException e) {
			logger.error("Acccount fetch fail");
			logger.error(e.getMessage());
		}
		return accounts;
	}
    
    public List<ClearingView> parseView(ResultSet rs){
    	 List<ClearingView> view_objs = new ArrayList<ClearingView>();
    	 try {
				while (rs.next()){
					   String jobId = rs.getString("JobId");
					   String orderId = rs.getString("OrderId");
					   String jobStatusCd = rs.getString("JobStatusCd");
					   String taskId = rs.getString("TaskId");
					   int taskIndex = rs.getInt("TaskIndex");
					   
					   String TaskFromAcc = rs.getString("taskFromAcc");
					   String TaskToAcc = rs.getString("TaskToAcc");
					   Double TaskAmount = rs.getDouble("TaskAmount");
					   String TaskCurrencyCd  = rs.getString("TaskCurrencyCd");
					   String TaskStatusCd  = rs.getString("TaskStatusCd");
					   long UserId  = rs.getLong("UserId");
					   String OrderFromAcc  = rs.getString("OrderFromAcc");
					   String OrderToAcc = rs.getString("OrderToAcc");
					   Double OrderAmount = rs.getDouble("OrderAmount");
					   Double FxRate = rs.getDouble("FxRate");
					   String FromCountryCd = rs.getString("FromCountryCd");
					   String ToCountryCd = rs.getString("ToCountryCd");
					   String ServiceTypeCd = rs.getString("ServiceTypeCd");
					   Double ServiceFee = rs.getDouble("ServiceFee");
					   int BankId = rs.getInt("BankId");
					   String BankCd = rs.getString("BankCd");
					   String BankName = rs.getString("BankName");
					   int PayMethodId = rs.getInt("PayMethodId");
					   String PayMethodName = rs.getString("PayMethodName");
					   String ServiceName = rs.getString("ServiceName");
					   Double GatewayFee = rs.getDouble("GatewayFee");
					   
					   
					   String jsonString = helper.getViewObj(TaskFromAcc, TaskToAcc, TaskAmount, TaskCurrencyCd, TaskStatusCd, UserId, OrderFromAcc, OrderToAcc, OrderAmount, FxRate, FromCountryCd, ToCountryCd, ServiceTypeCd, ServiceFee, BankId, BankCd, BankName, PayMethodId, PayMethodName, ServiceName, GatewayFee).toString();
					   
					   String encryption = EncryptionUtil.encrypt(jsonString);		   
					  
					   Timestamp jobUpdTime = rs.getTimestamp("JobUpdTime");
					   Timestamp orderUpdTime = rs.getTimestamp("OrderUpdTime");
					   Timestamp taskUpdTime = rs.getTimestamp("TaskUpdTime");
					   Timestamp addTime = rs.getTimestamp("addTime");
					   
					   ClearingView view = new ClearingView(jobId, orderId, jobStatusCd, taskId,taskIndex, encryption, jobUpdTime.getTime(), orderUpdTime.getTime(), taskUpdTime.getTime());
					   view_objs.add(view);
					   
				  }
				  
			} catch (SQLException e) {
				logger.error("View fetch fail");
				logger.error(e.getMessage());
			}
    	    return view_objs;
    }
    
    
    public List<GwTransaction> parseGw(ResultSet rs){
   	 List<GwTransaction> gw_objs = new ArrayList<GwTransaction>();
	 try {
			while (rs.next()){
				   String trxId = rs.getString("transactionId");
				   long userId = rs.getLong("userId");
				   String useruid = rs.getString("userUid");
				   String orderId = rs.getString("orderId");
				   String originInstance = rs.getString("originInstance");
				   String processInstance = rs.getString("processInstance");
				   String typeCd = rs.getString("typeCd");
				   String currency_ = rs.getString("currency_");
				   String amount = rs.getString("amount");
				   String fee = rs.getString("fee");
				   String statusCd = rs.getString("statusCd");
				   int retryTimes = rs.getInt("retryTimes");
				   Timestamp addTime = rs.getTimestamp("addTime");
				   Timestamp updateTime = rs.getTimestamp("updateTime");
				   String accDetailJson = rs.getString("acctDetailJson");
				   String senderInfoJson = rs.getString("senderInfoJson");
				   String processDetailJson = rs.getString("processDetailJson");
				   String callbackUrl = rs.getString("callbackUrl");
				   
				   String obj_json = helper.getGWJson(trxId, userId, useruid, orderId, originInstance, processInstance, typeCd, currency_, amount, fee, statusCd, retryTimes, addTime.getTime(), updateTime.getTime(), accDetailJson, senderInfoJson, processDetailJson, callbackUrl).toString();
				   String encryption = EncryptionUtil.encrypt(obj_json);
				   GwTransaction gw_obj = new GwTransaction(trxId, orderId, useruid, addTime.getTime(), updateTime.getTime() ,encryption);
			       gw_objs.add(gw_obj);  
			}
			  
		} catch (SQLException e) {
			logger.error("GW trasaction fetch fail");
			logger.error(e.getMessage());
		}
	    return gw_objs;
    }
}
