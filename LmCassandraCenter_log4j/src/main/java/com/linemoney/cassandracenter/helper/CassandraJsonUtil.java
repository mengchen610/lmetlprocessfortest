package com.linemoney.cassandracenter.helper;

import java.sql.Timestamp;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;


//Does this have to extends LmJsonUtil? cause in this way the method wont be static and I have to create an object each time
//But if I create obj each time. it feels wired cause its just util..
public class CassandraJsonUtil  {
	
	//static LmDateUtil dateUtil = new LmDateUtil();
	private static final Logger logger = Logger.getLogger(CassandraJsonUtil.class);

	 public String getOrderJSONObject(String orderId, long userId, 
			 String originInstance, String parentOrderId, String fromCurrencyCd, String toCurrencyCd,
			 String fromAccountId, String toAccountId, String fromCountryCd, String toCountryCd,
			 double fromAmount, String fxTypeCd, double fxRate, double baseRate, String serviceTypeCd,
			 double serviceFee, String recurringTypeCd, Timestamp orderTime, String durationTypeCd,
			 String processStatusCd, Timestamp processTime, String purpose, String active_flag,
			 String addUser,Timestamp addTime, String updateUser, Timestamp updateTime, String userId_uuid) throws JSONException {
		
		JSONObject jsonObject = new JSONObject();

		jsonObject.put("orderId", orderId );
		jsonObject.put("originInstance",originInstance);
		jsonObject.put("userId", userId);
		jsonObject.put("parentOrderId",parentOrderId);
		jsonObject.put("fromCurrencyCd", fromCurrencyCd);
		jsonObject.put("toCurrencyCd", toCurrencyCd);
		jsonObject.put("fromAccountId", fromAccountId);
		jsonObject.put("toAccountId", toAccountId);
		jsonObject.put("fromCountryCd", fromCountryCd);
		jsonObject.put("toCountryCd", toCountryCd);
		jsonObject.put("fromAmount", fromAmount);
		jsonObject.put("fxTypeCd", fxTypeCd);
		jsonObject.put("fxRate", fxRate);
		jsonObject.put("baseRate", baseRate);
		jsonObject.put("serviceTypeCd", serviceTypeCd);
		jsonObject.put("serviceFee",serviceFee);
		jsonObject.put("recurringTypeCd", recurringTypeCd);
		jsonObject.put("orderTime", orderTime);
		jsonObject.put("durationTypeCd", durationTypeCd);
		jsonObject.put("processStatusCd", processStatusCd);
		jsonObject.put("processTime", processTime);
		jsonObject.put("purpose",purpose);
		jsonObject.put("active_flag",active_flag);
		jsonObject.put("addUser", addUser);
		jsonObject.put("addTime", addTime);
		jsonObject.put("updateUser", updateUser);
		jsonObject.put("updateTime", updateTime);
		jsonObject.put("userId_uuid", userId_uuid);
		
		return jsonObject.toString();
	}

	 public String getTaskJSONObject(String fromAccountId,
				String fromAccountDetail, String toAccountId,
				String toAccountDetail, double fromAmount, String statusCd,
				String transactionTypeCd, int taskIndex, Timestamp addTime,
				Timestamp updateTime, double serviceFee) throws JSONException {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("fromAccountId", fromAccountId);
			jsonObject.put("fromAccountDetail", fromAccountDetail);
			jsonObject.put("toAccountId", toAccountId);
			jsonObject.put("toAccountDetail", toAccountDetail);
			jsonObject.put("fromAmount", fromAmount);
			jsonObject.put("statusCd", statusCd);
			jsonObject.put("transactionTypeCd", transactionTypeCd);
			jsonObject.put("taskIndex", taskIndex);
			jsonObject.put("addTime", addTime);
			jsonObject.put("updateTime", updateTime);
			jsonObject.put("serviceFee", serviceFee);
			return jsonObject.toString();
		}

	public String getUserJSONObject(String userId_uuid, long userId, long companyId,
			Timestamp createDate, Timestamp modifiedDate, int defaultUser, long contactId,
			String screenName, String emailAddress, long facebookId,
			String languageId, String timeZoneId, String comments,
			String firstName, String middleName, String lastName,
			String jobTitle, int lockout, Timestamp lockoutDate,
			int agreedToTermsOfUse, int emailAddressVerified, int status) throws JSONException {
		
		JSONObject jsonObject = new JSONObject();

		jsonObject.put("uuid_", userId_uuid);
		jsonObject.put("userId", userId);
		jsonObject.put("companyId", companyId);
		jsonObject.put("createDate", createDate);
		jsonObject.put("modifiedDate", modifiedDate);
		jsonObject.put("defaultUser", defaultUser);
		jsonObject.put("contactId", contactId);
		jsonObject.put("screenName", screenName);
		jsonObject.put("emailAddress", emailAddress);
		jsonObject.put("facebookId", facebookId);
		jsonObject.put("languageId", languageId);
		jsonObject.put("timeZoneId", timeZoneId);
		jsonObject.put("comments", comments);
		jsonObject.put("firstName", firstName);
		jsonObject.put("middleName", middleName);
		jsonObject.put("lastName", lastName);
		jsonObject.put("jobTitle", jobTitle);
		jsonObject.put("lockout", lockout);
		jsonObject.put("lockoutDate", lockoutDate);
		jsonObject.put("agreedToTermsOfUse", agreedToTermsOfUse);
		jsonObject.put("emailAddressVerified", emailAddressVerified);
		jsonObject.put("status", status);

		return jsonObject.toString();
	}
    
	public String getAccountJSONObject(String accountId, long userId,
			String accountHolderId, long bankId, String accountTypeCd,
			String bankName, String accountName, String detailJSON,
			String identity, String currencyCd, double balance,
			String paymentType, String password_, Timestamp expirationDate,
			String activeFlag, String addUser, Timestamp addTime,
			String updateUser, Timestamp updateTime) {
		// TODO Auto-generated method stub
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("accountId",accountId);
			jsonObject.put("userId",userId);
			jsonObject.put("accountHolderId",accountHolderId);
			jsonObject.put("bankId",bankId);
			jsonObject.put("accountTypeCd",accountTypeCd);
			jsonObject.put("bankName",bankName);
			jsonObject.put("accountName",accountName);
			jsonObject.put("detailJSON",detailJSON);
			jsonObject.put("identity",identity);
			jsonObject.put("currencyCd",currencyCd);
			jsonObject.put("balance",balance);
			jsonObject.put("paymentType",paymentType);
			jsonObject.put("password_",password_);
			jsonObject.put("expirationDate",expirationDate);
			jsonObject.put("activeFlag",activeFlag);
			jsonObject.put("addUser",addUser);
			jsonObject.put("addTime",addTime);
			jsonObject.put("updateUser",updateUser);
			jsonObject.put("updateTime",updateTime);
		} catch (JSONException e) {
		
			logger.error(e.getMessage());
		}

		return jsonObject.toString();
	}
    
	public String getAccountHolderJSONObject(String accountHolderId,
			long userId, String fullName, String lastName, String firstName,
			String middleName, String email, String addressLine1,
			String addressLine2, String addressLine3, String city,
			String state_, String zipCode, String countryCd, String phone,
			String identityNum, String activeFlag, String addUser,
			Timestamp addTime, String updateUser, Timestamp updateTime) {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("accountHolderId",accountHolderId);
			jsonObject.put("userId",userId);
			jsonObject.put("fullName",fullName);
			jsonObject.put("lastName",lastName);
			jsonObject.put("firstName",firstName);
			jsonObject.put("middleName",middleName);
			jsonObject.put("email",email);
			jsonObject.put("addressLine1",addressLine1);
			jsonObject.put("addressLine2",addressLine2);
			jsonObject.put("addressLine3", addressLine3);
			jsonObject.put("city",city);
			jsonObject.put("state_",state_);
			jsonObject.put("zipCode",zipCode);
			jsonObject.put("countryCd",countryCd);
			jsonObject.put("phone",phone);
			jsonObject.put("identityNum",identityNum);
			jsonObject.put("activeFlag",activeFlag);
			jsonObject.put("addUser",addUser);
			jsonObject.put("addTime",addTime);
			jsonObject.put("updateUser",updateUser);
			jsonObject.put("updateTime",updateTime);
		} catch (JSONException e) {
			logger.error(e.getMessage());
		}

		return jsonObject.toString();
		}

	public String getBankJSONObject(long bankId, String bankCd,
			String languageCd, String bankTypeCd, String bankName,
			String description, String bankSwift, String bankRouting,
			String addressLine1, String addressLine2, String addressLine3,
			String countryCd, String stateCd, String city, String postCode,
			String phone, String activeFlag, String addUser, Timestamp addTime,
			String updateUser, Timestamp updateTime, String currencyCd,
			String payMethod_obj) {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("bankId",bankId);
			jsonObject.put("bankCd",bankCd);
			jsonObject.put("languageCd",languageCd);
			jsonObject.put("bankTypeCd",bankTypeCd);
			jsonObject.put("bankName",bankName);
			jsonObject.put("description",description);
			jsonObject.put("bankSwift",bankSwift);
			jsonObject.put("bankRouting",bankRouting);
			jsonObject.put("addressLine1",addressLine1);
			jsonObject.put("addressLine2",addressLine2);
			jsonObject.put("addressLine3",addressLine3);
			jsonObject.put("countryCd",countryCd);
			jsonObject.put("stateCd",stateCd);
			jsonObject.put("city",city);
			jsonObject.put("postCode",postCode);
			jsonObject.put("phone",phone);
			jsonObject.put("activeFlag",activeFlag);
			jsonObject.put("addUser",addUser);
			jsonObject.put("addTime",addTime);
			jsonObject.put("updateUser",updateUser);
			jsonObject.put("updateTime",updateTime);
			jsonObject.put("currencyCd",currencyCd);
			jsonObject.put("payMethod_obj",payMethod_obj);
		} catch (JSONException e) {
			logger.error(e.getMessage());
		}
		return jsonObject.toString();		
	}

	public String getPaymentMethodJSONObject(int payMethodId,
			String payMethodName, String serviceName, String currencyCd,
			String countryCd, String identity, String description,
			Timestamp updateTime) {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("payMethodId",payMethodId);
			jsonObject.put("payMethodName",payMethodName);
			jsonObject.put("serviceName",serviceName);
			jsonObject.put("currencyCd",currencyCd);
			jsonObject.put("countryCd",countryCd);
			jsonObject.put("identity",identity);
			jsonObject.put("description",description);
			jsonObject.put("updateTime",updateTime);
		} catch (JSONException e) {
			logger.error(e.getMessage());
		}
		return jsonObject.toString();		
	}

	public org.json.JSONObject getViewObj(String TaskFromAcc, String TaskToAcc, Double TaskAmount, String TaskCurrencyCd, 
								  String TaskStatusCd, long UserId,String OrderFromAcc,
								  String OrderToAcc, Double OrderAmount, Double FxRate,
								  String FromCountryCd, String ToCountryCd, String ServiceTypeCd,
								  Double ServiceFee, int BankId, String BankCd ,String BankName,
								  int PayMethodId, String PayMethodName,String ServiceName, Double GatewayFee) {
							    
		    org.json.JSONObject jsonObject = new org.json.JSONObject();
		    
		    try {
				jsonObject.put("TaskFromAcc", TaskFromAcc);
				jsonObject.put("TaskToAcc",TaskToAcc);
			    jsonObject.put("TaskAmount", TaskAmount);
			    jsonObject.put("TaskCurrencyCd", TaskCurrencyCd);
			    jsonObject.put("TaskStatusCd", TaskStatusCd);
			    jsonObject.put("UserId", UserId);
			    jsonObject.put("OrderFromAcc", OrderFromAcc);
			    jsonObject.put("OrderToAcc", OrderToAcc);
			    jsonObject.put("OrderAmount", OrderAmount);
			    jsonObject.put("FxRate", FxRate);
			    jsonObject.put("FromCountryCd", FromCountryCd);
			    jsonObject.put("ToCountryCd",ToCountryCd);
			    jsonObject.put("ServiceTypeCd", ServiceTypeCd);
			    jsonObject.put("ServiceFee", ServiceFee);
			    jsonObject.put("BankId", BankId);
			    jsonObject.put("BankCd", BankCd);
			    jsonObject.put("BankName", BankName);
			    jsonObject.put("PayMethodId", PayMethodId);
			    jsonObject.put("PayMethodName", PayMethodName);
			    jsonObject.put("ServiceName", ServiceName);
			    jsonObject.put("GatewayFee", GatewayFee);
			} catch (JSONException e) {
				logger.error(e.getMessage());
			}
		    
		    
		   
		    return jsonObject;
	}
	
	
	public org.json.JSONObject getGWJson(String trxId,long userId,String uuid, String orderId, String origin,String process,
			  				String typeCd, String currency_, String amount, String fee, String statusCd, int retryTimes, 
			  				long addTime, long updateTime, String acctDetail, String senderInfo, String processDetail, String callback ){
		
		org.json.JSONObject jsonObject = new org.json.JSONObject();
		
		try{
			jsonObject.put("transactionId", trxId);
			jsonObject.put("orderId", orderId);
			jsonObject.put("userId", userId);
			jsonObject.put("userUid", uuid);
			jsonObject.put("originInstance", origin);
			jsonObject.put("processInstance", process);
			jsonObject.put("typeCd", typeCd);
			jsonObject.put("currecy_", currency_);
			jsonObject.put("amount", amount);
			jsonObject.put("fee",fee);
			jsonObject.put("statusCd", statusCd);
			jsonObject.put("retryTimes", retryTimes);
			jsonObject.put("addTime", addTime);
			jsonObject.put("updateTime", updateTime);
			jsonObject.put("acctDetailJson", acctDetail);
			jsonObject.put("senderInfoJson", senderInfo);
			jsonObject.put("processDetailJson", processDetail);
			jsonObject.put("callbackUrl",callback);
		}catch(JSONException e){
			logger.error(e.getMessage());
		}
		return jsonObject;
		
	}
}
