package com.linemoney.cassandracenter.helper;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class LmCassandraCenterConstants {
	
	public static String URL;
    public static String DATABASE;
    public static String USER;
    public static String PASSWORD;
    public static String DRIVER_CLASS;
    public static String[] CONTACT_POINTS;
    public static String CASSANDRAUSERNAME;
    public static String CASSANDRAPASSWORD;
    public static int PORT;
    public static String KEYSPACE;
    public static String SERVERCOUNTRY;
    private static final Logger logger = Logger.getLogger(LmCassandraCenterConstants.class);
	
	static {
		Properties prop = new Properties();
		InputStream input = null;
		
		try {
			
			//ClassLoader classLoader = getClass().getClassLoader();
			input = new FileInputStream("/apps/ETL.properties");
			// load a properties file
			prop.load(input);

			// get the property value and print it out
			URL = prop.getProperty("jdbc.url").trim();
   			DATABASE = prop.getProperty("jdbc.database").trim();
   			USER = prop.getProperty("jdbc.username").trim();
   			PASSWORD = prop.getProperty("jdbc.password").trim();
   			DRIVER_CLASS = prop.getProperty("jdbc.driverClassName");
   			
   			
   			CONTACT_POINTS = prop.getProperty("cassandra.contactpoints").split(";");
   		    CASSANDRAUSERNAME = prop.getProperty("cassandra.user");
   		    CASSANDRAPASSWORD = prop.getProperty("cassandra.password");
   		    KEYSPACE = prop.getProperty("cassandra.keyspace");
   		    String port = prop.getProperty("cassandra.port");
   		    PORT = Integer.parseInt(port);
   		    
   		    SERVERCOUNTRY = prop.getProperty("server.country");
   		    logger.info("URL :" + URL);
   		    logger.info("DATABASE :" + DATABASE);
   		    logger.info("USER :" + USER);
   		    logger.info("PASSWORD :" + PASSWORD);
   		    logger.info("DRIVER_CLASS :" + DRIVER_CLASS);
   		    logger.info("KEYSPACE :" + KEYSPACE);
   		
		} catch (IOException ex) {
			logger.error(ex.getMessage());
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					logger.error("Inputstream Close Fail");
					logger.error(e.getMessage());
				}
			}
		}

	  }
}
