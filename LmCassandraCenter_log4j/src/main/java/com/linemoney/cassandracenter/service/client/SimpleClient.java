package com.linemoney.cassandracenter.service.client;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

import javax.swing.UIDefaults;

import org.apache.log4j.Logger;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Host;
import com.datastax.driver.core.Metadata;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.Statement;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.linemoney.cassandracenter.helper.LmCassandraCenterConstants;
import com.linemoney.cassandracenter.model.*;
import com.linemoney.util.security.EncryptionUtil;

public class SimpleClient {
	
	private static SimpleClient instance;
	private Cluster cluster;
	private Session session;
	private static final String keyspace = LmCassandraCenterConstants.KEYSPACE;
	
	private String insertOrder = "INSERT INTO "+keyspace+".lm_order"
	        + "(orderId, userId, obj, orderTime) "
	        + "VALUES (?, ?, ?, ?)";
	
	private String insertOrderStatus = "INSERT INTO "+keyspace+".lm_order_status"
            + "(orderId, status, updatetime) "
            + "VALUES (?, ?, ?)";
	 
	private String insertJob = "INSERT INTO "+keyspace+".lm_job"
			+ "(jobId, statusCd, addTime, updatetime, order_list) "
			+ "VALUES (?, ?, ?, ?, ?)";
	
	private String insertTask= "INSERT INTO "+keyspace+".lm_task"
			+ "(taskId, orderId, obj) "
			+ "VALUES (?, ?, ?)";
	
	private String insertTransaction= "INSERT INTO "+keyspace+".lm_transaction"
		    + "(transactionId, taskId, statusCd, addTime) "
		    + "VALUES (?, ?, ?, ?)";
	
	private String insertUser = "INSERT INTO "+keyspace+".lm_user"
			+ "(userId, languageId, obj) "
			+ "VALUES (?, ?, ?)";
	
	private String insertAccount= "INSERT INTO "+keyspace+".lm_account"
			+ "(accountId, userId, holder_obj, bank_obj, accountTypeCd, account_obj) "
			+ "VALUES (?, ?, ?, ?, ?, ?)";
	
	private String insertView = "INSERT INTO "+keyspace+".cassandra_view "
			+ "(jobId, orderId, jobStatusCd, taskId, taskIndex, view_obj, jobUpdTime, orderUpdTime, taskUpdTime) "
			+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
	private String insertGw = "INSERT INTO "+keyspace+".cassandra_gw_trx "
			+ "(trxId, orderId, userId, trx_obj, addTime, updateTime) "
			+ "VALUES (?, ?, ?, ?, ?, ?)";
	private PreparedStatement orderStatement;
	private PreparedStatement orderStatusStatement;
	private PreparedStatement jobStatement;
	private PreparedStatement taskStatement;
	private PreparedStatement transactionStatement;
	private PreparedStatement userStatement;
	private PreparedStatement accountStatement;
	private PreparedStatement viewStatement;
	private PreparedStatement gwStatement;

    
	private static final Logger logger = Logger.getLogger(SimpleClient.class);
	
	
	private SimpleClient(){}
	
	public static SimpleClient getInstance(){
		   if (instance == null){
			   instance = new SimpleClient();
		   }
		   return instance;
	}
	
	public void connect() {
		cluster = Cluster.builder().addContactPoints(LmCassandraCenterConstants.CONTACT_POINTS)
				  .withPort(9042)
				  .withCredentials(LmCassandraCenterConstants.CASSANDRAUSERNAME, LmCassandraCenterConstants.CASSANDRAPASSWORD).build();
		
		Metadata metadata = cluster.getMetadata();
		logger.info("Connected to cluster:" + metadata.getClusterName());
		for (Host host : metadata.getAllHosts()) {
			logger.info("Datatacenter: " + host.getDatacenter()
					+ "; Host: " + host.getAddress() + "; Rack: "
					+ host.getRack());
		}
	}
	
	public void prepareStatement(){
		if (session == null || session.isClosed()){
        	getSession();
        	logger.info("Start Loading, open session first time for preparestatement");
        }
		orderStatement = session.prepare(insertOrder);
		orderStatusStatement = session.prepare(insertOrderStatus);
		jobStatement = session.prepare(insertJob);
		taskStatement = session.prepare(insertTask);
		transactionStatement = session.prepare(insertTransaction);
		accountStatement = session.prepare(insertAccount);
		userStatement = session.prepare(insertUser);
		viewStatement = session.prepare(insertView);
		gwStatement = session.prepare(insertGw);
		closeSession();
	}

	private void getSession() {
		session = cluster.connect();
		
	}

	private void closeSession() {
		session.close();
	}

	public void close() {
		cluster.close();
	}
	

	public void loadViewData(ClearingView view_obj) {
        if (session == null || session.isClosed()){
        	getSession();
        	
        }
		/*PreparedStatement statement = session
				.prepare("INSERT INTO "+keyspace+".cassandra_view "
						+ "(jobId, orderId, jobStatusCd, taskId, taskIndex, view_obj, jobUpdTime, orderUpdTime, taskUpdTime) "
						+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
		*/
        String jobId = view_obj.getJobId();
        String orderId = view_obj.getOrderId();
        String jobStatusCd = view_obj.getJobStatusCd();
        String taskId = view_obj.getTaskId();
        int taskIndex = view_obj.getTaskIndex();
        Timestamp jobUpdTime = new Timestamp(view_obj.getJobUpdTime());
        Timestamp orderUpdTime = new Timestamp(view_obj.getOrderUpdTime());
        Timestamp taskUpdTime = new Timestamp(view_obj.getTaskUpdTime());
        		
		BoundStatement boundStatement = viewStatement.bind(
				jobId,orderId,jobStatusCd,taskId,taskIndex,view_obj.getView_obj(),jobUpdTime,orderUpdTime,taskUpdTime);
		
		ResultSet res = session.execute(boundStatement);
		closeSession();
		
	}
	
	public void loadOrderData(Order order) {
		if (session == null || session.isClosed()){
        	getSession();
        }
		/*PreparedStatement statement = session
				.prepare("INSERT INTO "+keyspace+".lm_order"
						+ "(orderId, userId, obj, orderTime) "
						+ "VALUES (?, ?, ?, ?)");*/
		
		Timestamp time = new Timestamp(order.getOrderTime());
		UUID userId = UUID.fromString(order.getUserId());
		
		BoundStatement boundStatement = orderStatement.bind(
				order.getOrderId(),userId,order.getObj(),time);
		
		ResultSet rs = session.execute(boundStatement);
		closeSession();
	}
	
	public void loadOrderStatusData(Order order) {
		if (session == null || session.isClosed()){
        	getSession();
        }
		/*PreparedStatement statement = session
				.prepare("INSERT INTO "+keyspace+".lm_order_status"
						+ "(orderId, status, updatetime) "
						+ "VALUES (?, ?, ?)");
		*/
		Timestamp time = new Timestamp(order.getUpdateTime());
		BoundStatement boundStatement = orderStatusStatement.bind(
				order.getOrderId(),order.getStatus(), time);
		
		session.execute(boundStatement);
		closeSession();
	}
	
	public void loadjobData(Job job) {
		if (session == null || session.isClosed()){
        	getSession();
        }
		/*PreparedStatement statement = session
				.prepare("INSERT INTO "+keyspace+".lm_job"
						+ "(jobId, statusCd, addTime, updatetime, order_list) "
						+ "VALUES (?, ?, ?, ?, ?)");
		*/
		Timestamp addTime = new Timestamp(job.getAddTime());
		Timestamp updTime = new Timestamp(job.getUpdateTime());
		List<String> order_list = job.getOrder_list();
		BoundStatement boundStatement = jobStatement.bind(
				job.getJobId(),job.getStatusCd(), addTime, updTime, order_list);
		
		session.execute(boundStatement);
		closeSession();
		
	}
	
	public void loadTaskData(Task task) {
		if (session == null || session.isClosed()){
        	getSession();
        }
		/*PreparedStatement statement = session
				.prepare("INSERT INTO "+keyspace+".lm_task"
						+ "(taskId, orderId, obj) "
						+ "VALUES (?, ?, ?)");*/
        
		BoundStatement boundStatement = taskStatement.bind(
				task.getTaskId(), task.getOrderId(), task.getText());
		
		session.execute(boundStatement);
		closeSession();
	}
	
	public void loadTransactionData(Transaction transaction) {
		if (session == null || session.isClosed()){
        	getSession();
        }
		/*PreparedStatement statement = session
				.prepare("INSERT INTO "+keyspace+".lm_transaction"
						+ "(transactionId, taskId, statusCd, addTime) "
						+ "VALUES (?, ?, ?, ?)");*/
		
		Timestamp addTime = new Timestamp(transaction.getAddTime());
		BoundStatement boundStatement = transactionStatement.bind(
				transaction.getTransactionId(), transaction.getTaskId(), transaction.getStatusCd(), addTime);
		
		session.execute(boundStatement);
		closeSession();
	}
	
	public void loadUserData(User user) {
		if (session == null || session.isClosed()){
        	getSession();
        }
		/*PreparedStatement statement = session
				.prepare("INSERT INTO "+keyspace+".lm_user"
						+ "(userId, languageId, obj) "
						+ "VALUES (?, ?, ?)");*/
		UUID userId = UUID.fromString(user.getUserId_uuid());
		
		BoundStatement boundStatement = userStatement.bind(
				userId, user.getLanguageId(), user.getObj());
		session.execute(boundStatement);
		closeSession();
		
	}
	
	
	public void loadAccountData(Account a) {
		if (session == null || session.isClosed()){
        	getSession();
        }
		/*PreparedStatement statement = session
				.prepare("INSERT INTO "+keyspace+".lm_account"
						+ "(accountId, userId, holder_obj, bank_obj, accountTypeCd, account_obj) "
						+ "VALUES (?, ?, ?, ?, ?, ?)");*/
		
		UUID userId = UUID.fromString(a.getUserId());
		BoundStatement boundStatement = accountStatement.bind(
				a.getAccountId(), userId, a.getHolder_obj(), a.getBank_obj(), a.getAccountTypeCd(), a.getAccount_obj());
		
		session.execute(boundStatement);
		closeSession();
	}
	
	public void loadGwData(GwTransaction gw) {
		if (session == null || session.isClosed()){
        	getSession();
        }
		/*PreparedStatement statement = session
				.prepare("INSERT INTO "+keyspace+".lm_account"
						+ "(accountId, userId, holder_obj, bank_obj, accountTypeCd, account_obj) "
						+ "VALUES (?, ?, ?, ?, ?, ?)");*/
		UUID userId = null;
		try{
		userId = UUID.fromString(gw.getUuid());
		} catch(IllegalArgumentException e){
			userId = new UUID(10, 1);
			logger.error(e.getMessage());
		}
		Timestamp addTime = new Timestamp(gw.getAddTime());
		Timestamp updateTime = new Timestamp(gw.getUpdateTime());
		BoundStatement boundStatement = gwStatement.bind(
			           gw.getTrxId(), gw.getOrderId(), userId, gw.getObj(), addTime, updateTime);
		
		session.execute(boundStatement);
		closeSession();
	}
	
	public void querySchema(String keyspace, String table) {
		if (session == null || session.isClosed()){
        	getSession();
        }
		Statement statement = QueryBuilder.select().all().from(keyspace, table);
		ResultSet results = session.execute(statement);
		
		for (Row row : results) {
			   String jobId = row.getString("JobId");
			   String orderId = row.getString("OrderId");
			   String jobStatusCd = row.getString("JobStatusCd");
			   String taskId = row.getString("TaskId");
			   int taskIndex = row.getInt("TaskIndex");
			   
			   String view_obj = row.getString("view_obj");
			   String decrption = EncryptionUtil.decrypt(view_obj);
			   System.out.println(decrption);
		}
		closeSession();
	}
	
	
		
}
