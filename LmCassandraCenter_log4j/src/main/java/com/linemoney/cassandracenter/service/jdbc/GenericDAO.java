package com.linemoney.cassandracenter.service.jdbc;

import java.sql.SQLException;
import java.util.List;



public abstract class  GenericDAO<T> {

    public abstract  List<T> selectAll() throws SQLException; 
    
    public abstract List<T> selectUpdateAll(long filterTime) throws SQLException;
    
    protected final String tableName;
    protected ConnectionFactory pool;
    
    public GenericDAO(ConnectionFactory pool, String tableName) {
    	this.pool = pool;
		this.tableName = tableName;
}

}