package com.linemoney.cassandracenter.service.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;




import org.apache.log4j.Logger;

import com.linemoney.cassandracenter.helper.LmCassandraCenterConstants;



public class ConnectionFactory {
    //static reference to itself
    private static ConnectionFactory instance;
    private static final Logger logger = Logger.getLogger(ConnectionFactory.class);
    private String driver = LmCassandraCenterConstants.DRIVER_CLASS;
   
    //private constructor
    private ConnectionFactory() {
     try{
            Class.forName(LmCassandraCenterConstants.DRIVER_CLASS);
        } catch (ClassNotFoundException e) {
            logger.error("ConnectionFactory init problem : " + e.getMessage());
        }
    }
     
    public static ConnectionFactory getInstance(){
    	   if (instance == null){
    		   instance = new ConnectionFactory();
    	   }
    	   return instance;
    }
    
    public Connection getConnection() {
        Connection connection = null;
        try {
        	System.out.println(LmCassandraCenterConstants.URL);
            connection = DriverManager.getConnection(LmCassandraCenterConstants.URL, LmCassandraCenterConstants.USER, LmCassandraCenterConstants.PASSWORD);
        } catch (SQLException e) {
        	logger.error("Unable to Connect to Database.");
        }
        return connection;
    }   
     
    
}
