package com.linemoney.cassandracenter.service.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONException;

import com.linemoney.cassandracenter.helper.ConvertHelper;
import com.linemoney.cassandracenter.helper.LmCassandraCenterConstants;
import com.linemoney.cassandracenter.model.Transaction;


public class TransactionDao extends GenericDAO<Transaction>{
	
	private ConvertHelper helper;
	
	private static final Logger logger = Logger.getLogger(TransactionDao.class);
	
	public TransactionDao(ConnectionFactory pool, String tableName) {
			super(pool, tableName);
			helper =  new ConvertHelper();
	}

	
	public List<Transaction> selectAll() throws SQLException {
		Connection con = pool.getConnection();
		 List<Transaction> transactions = new ArrayList<Transaction>();
		 ResultSet rs = null;
  	     String selectQuery = "select * from "+ tableName;
  	     try {
			PreparedStatement ps= con.prepareStatement(selectQuery);
			
			rs = ps.executeQuery();
			transactions = helper.parseTransaction(rs);
		  } catch (SQLException e) {
			  logger.error(e.getMessage());
		  } catch (JSONException e) {
			  logger.error(e.getMessage());
		}
  	      con.close();
  	      return transactions;	
	}


	@Override
	public List<Transaction> selectUpdateAll(long filterTime)
			throws SQLException {
		Connection con = pool.getConnection();
		List<Transaction> transactions  = new ArrayList<Transaction>();
		ResultSet rs = null;
		String query = "select * from " + tableName + " where addTime >= ?";
		try {
			PreparedStatement ps = con.prepareStatement(query);

			java.sql.Timestamp ftime = new java.sql.Timestamp(filterTime);
			ps.setTimestamp(1, ftime);

			rs = ps.executeQuery();
			transactions = helper.parseTransaction(rs);
		} catch (SQLException e) {
			logger.error(e.getMessage());
		} catch (JSONException e) {
			logger.error(e.getMessage());
		}
		con.close();
		return transactions;
	}

	
	
     
}
