package com.linemoney.cassandracenter.service.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONException;

import com.linemoney.cassandracenter.helper.ConvertHelper;
import com.linemoney.cassandracenter.helper.LmCassandraCenterConstants;
import com.linemoney.cassandracenter.model.User;

public class UserDAO extends GenericDAO<User> {

	private ConvertHelper helper;
	
	private static final Logger logger = Logger.getLogger(UserDAO.class);
	
	
	public UserDAO(ConnectionFactory pool, String tableName) {
		super(pool, tableName);
		helper = new ConvertHelper();
		
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<User> selectAll() throws SQLException {
		
		 Connection con = pool.getConnection();
		 List<User> users = new ArrayList<User>();
		 ResultSet rs = null;
  	     String selectQuery = "select * from "+tableName;
  	     try {
			PreparedStatement ps= con.prepareStatement(selectQuery);
			
			rs = ps.executeQuery();
			users = helper.parseUser(rs);
		  } catch (SQLException e) {
			  logger.error(e.getMessage());
		  } catch (JSONException e) {
			  logger.error(e.getMessage());
		}
  	      con.close();
  	      return users;
	}

	@Override
	public List<User> selectUpdateAll(long filterTime) throws SQLException {
		
		 Connection con = pool.getConnection();
		 List<User> users = new ArrayList<User>();
		 ResultSet rs = null;
  	     String selectQuery = "select * from "+tableName + " where modifiedDate >= ?";
  	     try {
			PreparedStatement ps= con.prepareStatement(selectQuery);
			java.sql.Timestamp ftime = new java.sql.Timestamp(filterTime);
			ps.setTimestamp(1, ftime);
			
			rs = ps.executeQuery();
			users = helper.parseUser(rs);
		  } catch (SQLException e) {
			  logger.error(e.getMessage());
		  } catch (JSONException e) {
			  logger.error(e.getMessage());
		}
  	      con.close();
  	      return users;
	}
   
}
