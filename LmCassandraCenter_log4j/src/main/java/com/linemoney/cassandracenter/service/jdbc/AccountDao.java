package com.linemoney.cassandracenter.service.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONException;

import com.linemoney.cassandracenter.helper.ConvertHelper;
import com.linemoney.cassandracenter.helper.LmCassandraCenterConstants;
import com.linemoney.cassandracenter.model.Account;

public class AccountDao extends GenericDAO<Account> {

	private ConvertHelper helper;
	
	private static final Logger logger = Logger.getLogger(AccountDao.class);

	public AccountDao(ConnectionFactory pool, String tableName) {
		super(pool, tableName);
		helper = new ConvertHelper();
	}

	public List<Account> selectAll() throws SQLException {
		Connection con = pool.getConnection();
		List<Account> accounts = new ArrayList<Account>();
		ResultSet rs = null;
		String selectQuery = "select t1.*,"
				+ "t2.uuid_ as userId_uuid,"
				+ "t3.accountHolderId as accountHolder_accountHolderId, t3.userId as accountHolder_userId, t3.fullName as accountHolder_fullName,"
				+ "t3.lastName as accountHolder_lastName, t3.firstName as accountHolder_firstName,t3.middleName as accountHolder_middleName,"
				+ "t3.email as accountHolder_email,t3.addressLine1 as accountHolder_addressLine1, t3.addressLine2 as accountHolder_addressLine2,"
				+ "t3.addressLine3 as accountHolder_addressLine3, t3.city as accountHolder_city,t3.state_ as accountHolder_state_, t3.zipCode as accountHolder_zipCode,"
				+ "t3.countryCd as accountHolder_countryCd, t3.phone as accountHolder_phone, t3.identityNum as accountHolder_identityNum,"
				+ "t3.activeFlag as accountHolder_activeFlag, t3.addUser as accountHolder_addUser, t3.addTime as accountHolder_addTime,"
				+ "t3.updateUser as accountHolder_updateUser, t3.updateTime as accountHolder_updateTime,"
				+ "t4.bankId as bank_bankId, t4.bankCd as bank_bankCd, t4.languageCd as bank_languageCd, t4.bankTypeCd as bank_bankTypeCd,"
				+ "t4.bankName as bank_bankName, t4.description as bank_description, t4.bankSwift as bank_bankSwift, t4.bankRouting as bank_bankRouting,"
				+ "t4.addressLine1 as bank_addressLine1, t4.addressLine2 as bank_addressLine2, t4.addressLine3 as bank_addressLine3, t4.countryCd as bank_countryCd,"
				+ "t4.stateCd as bank_stateCd, t4.city as bank_city, t4.postCode as bank_postCode,t4.phone as bank_phone, t4.activeFlag as bank_activeFlag,"
				+ "t4.addUser as bank_addUser, t4.addTime as bank_addTime, t4.updateUser as bank_updateUser, t4.updateTime as bank_updateTime,"
				+ "t4.currencyCd as bank_currencyCd, t4.payMethodId as bank_payMethodId,"
				+ "t5.payMethodId as payMethod_payMethodId, t5.payMethodName as payMethod_payMethodName, t5.serviceName as payMethod_serviceName,"
				+ "t5.currencyCd as payMethod_currencyCd, t5.countryCd as payMethod_countryCd, t5.identity as payMethod_identity, t5.description as payMethod_description,"
				+ "t5.updateTime as payMethod_updateTime "
				+ "from "
				+ tableName
				+ " as t1 "
				+ "inner join User_ as t2 on t1.userId = t2.userId "
				+ "left join lm_accountHolder as t3 on t1.accountHolderId = t3.accountHolderId "
				+ "left join lm_bank as t4 on t1.bankId = t4.bankId "
				+ "left join lm_paymentmethod as t5 on t4.payMethodId = t5.payMethodId "
				+ "where t1.accountId like '"
 	    		+ LmCassandraCenterConstants.SERVERCOUNTRY+"%'";
		try {
			PreparedStatement ps = con.prepareStatement(selectQuery);

			rs = ps.executeQuery();
			accounts = helper.parseAccount(rs);
		} catch (SQLException e) {
			logger.error(e.getMessage());
		} catch (JSONException e) {
			logger.error(e.getMessage());
		}
        con.close();
		return accounts;
	}

	public List<Account> selectUpdateAll(long filterTime) throws SQLException {
		Connection con = pool.getConnection();
		List<Account> accounts = new ArrayList<Account>();
		ResultSet rs = null;
		String query = "select t1.*,"
				+ "t2.uuid_ as userId_uuid, "
				+ "t3.accountHolderId as accountHolder_accountHolderId, t3.userId as accountHolder_userId,t3.fullName as accountHolder_fullName,"
				+ "t3.lastName as accountHolder_lastName, t3.firstName as accountHolder_firstName,t3.middleName as accountHolder_middleName,"
				+ "t3.email as accountHolder_email,t3.addressLine1 as accountHolder_addressLine1, t3.addressLine2 as accountHolder_addressLine2,"
				+ "t3.addressLine3 as accountHolder_addressLine3, t3.city as accountHolder_city,t3.state_ as accountHolder_state_, t3.zipCode as accountHolder_zipCode,"
				+ "t3.countryCd as accountHolder_countryCd, t3.phone as accountHolder_phone, t3.identityNum as accountHolder_identityNum,"
				+ "t3.activeFlag as accountHolder_activeFlag, t3.addUser as accountHolder_addUser, t3.addTime as accountHolder_addTime,"
				+ "t3.updateUser as accountHolder_updateUser, t3.updateTime as accountHolder_updateTime,"
				+ "t4.bankId as bank_bankId, t4.bankCd as bank_bankCd, t4.languageCd as bank_languageCd, t4.bankTypeCd as bank_bankTypeCd,"
				+ "t4.bankName as bank_bankName, t4.description as bank_description, t4.bankSwift as bank_bankSwift, t4.bankRouting as bank_bankRouting,"
				+ "t4.addressLine1 as bank_addressLine1, t4.addressLine2 as bank_addressLine2, t4.addressLine3 as bank_addressLine3, t4.countryCd as bank_countryCd,"
				+ "t4.stateCd as bank_stateCd, t4.city as bank_city, t4.postCode as bank_postCode,t4.phone as bank_phone, t4.activeFlag as bank_activeFlag,"
				+ "t4.addUser as bank_addUser, t4.addTime as bank_addTime, t4.updateUser as bank_updateUser, t4.updateTime as bank_updateTime,"
				+ "t4.currencyCd as bank_currencyCd, t4.payMethodId as bank_payMethodId,"
				+ "t5.payMethodId as payMethod_payMethodId, t5.payMethodName as payMethod_payMethodName, t5.serviceName as payMethod_serviceName,"
				+ "t5.currencyCd as payMethod_currencyCd, t5.countryCd as payMethod_countryCd, t5.identity as payMethod_identity, t5.description as payMethod_description,"
				+ "t5.updateTime as payMethod_updateTime "
				+ "from "
				+ tableName
				+ " as t1 "
				+ "inner join user_ as t2 on t1.userId = t2.userId "
				+ "left join lm_accountHolder as t3 on t1.accountHolderId = t3.accountHolderId "
				+ "left join lm_bank as t4 on t1.bankId = t4.bankId "
				+ "left join lm_paymentmethod as t5 on t4.payMethodId = t5.payMethodId "
				+ "where t1.updateTime >= ? "
				+ "and t1.accountId like '"
 	    		+  LmCassandraCenterConstants.SERVERCOUNTRY+"%'";
		try {
			PreparedStatement ps = con.prepareStatement(query);

			java.sql.Timestamp ftime = new java.sql.Timestamp(filterTime);
			ps.setTimestamp(1, ftime);

			rs = ps.executeQuery();
			accounts = helper.parseAccount(rs);
		} catch (SQLException e) {
			logger.error(e.getMessage());
		} catch (JSONException e) {
			logger.error(e.getMessage());
		}
		con.close();
		return accounts;
	}

}
