package com.linemoney.cassandracenter.service.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.linemoney.cassandracenter.helper.ConvertHelper;
import com.linemoney.cassandracenter.model.ClearingView;



public class CassandraDAO extends GenericDAO<ClearingView> {
	
	   private ConvertHelper helper;
	
	   private static final Logger logger = Logger.getLogger(CassandraDAO.class);
	

	    public CassandraDAO(ConnectionFactory pool, String tableName) {
			super(pool, tableName);
			helper = new ConvertHelper();
	    }

		@Override
		public List<ClearingView> selectAll() throws SQLException {
			 List<ClearingView> objs = new ArrayList<ClearingView>();
			 Connection con = pool.getConnection();
	    	 ResultSet rs = null;
	    	 String selectQuery = "select * from " + tableName; 
	    	   try {
				PreparedStatement ps= con.prepareStatement(selectQuery);
				
			    rs = ps.executeQuery();
				objs = helper.parseView(rs);
			   } catch (SQLException e) {
					logger.error(e.getMessage());
			   }
	    	   
	    	   
	    	   try {
				con.close();
			   } catch (SQLException e) {
				   logger.error(e.getMessage());
			   }
	    	   
	    	   return objs;
		}

		@Override
		public List<ClearingView> selectUpdateAll(long filterTime){
			    List<ClearingView> objs = new ArrayList<ClearingView>();
				Connection con = pool.getConnection();
		    	ResultSet rs = null;
			    String selectQuery = "select * from " +tableName+" where addTime >= ?;"; 
			   try {
				PreparedStatement ps= con.prepareStatement(selectQuery);
				java.sql.Timestamp ftime = 	new java.sql.Timestamp(filterTime);
			    ps.setTimestamp(1, ftime);
				
			    rs = ps.executeQuery();
			    objs = helper.parseView(rs);
			    con.close();
			   } catch (SQLException e) {
				   logger.error(e.getMessage());
			   }
			   
			   return objs;
		}
	

}
