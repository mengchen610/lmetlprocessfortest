package com.linemoney.cassandracenter.service.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONException;

import com.linemoney.cassandracenter.helper.ConvertHelper;
import com.linemoney.cassandracenter.helper.LmCassandraCenterConstants;
import com.linemoney.cassandracenter.model.GwTransaction;
import com.linemoney.cassandracenter.model.Job;
import com.linemoney.cassandracenter.model.Order;

public class GwTransactionDAO extends GenericDAO<GwTransaction> {
	

	private ConvertHelper helper;
	
	private static final Logger logger = Logger.getLogger(JobDAO.class);

	public GwTransactionDAO(ConnectionFactory pool, String tableName) {
		super(pool, tableName);
		helper = new ConvertHelper();
	}
	
	@Override
	public List<GwTransaction>  selectAll() throws SQLException {
		 Connection con = pool.getConnection();
		 List<GwTransaction> trxs = new ArrayList<GwTransaction>();
		 ResultSet rs = null;
		 // TODO
  	     String selectQuery = "select * from " + tableName
  	    		+ " as t1 where length(t1.orderId) <= 5 and statusCd = 'SUCCESS';";
  	     try {
			PreparedStatement ps= con.prepareStatement(selectQuery);
			
			rs = ps.executeQuery();
			trxs = helper.parseGw(rs);
			
		  } catch (SQLException e) {
				logger.error(e.getMessage());
		  }
  	      con.close();
  	      return trxs;
	}

	@Override
	public List<GwTransaction> selectUpdateAll(long filterTime) throws SQLException {
		Connection con = pool.getConnection();
		List<GwTransaction> trxs = new ArrayList<GwTransaction>();
		 ResultSet rs = null;
		 String selectQuery = "select * from " + tableName
	  	    				+ " as t1 where length(t1.orderId) <= 5 and statusCd = 'SUCCESS' "
	  	    				+ "and updateTime >= ?;";
 	     try {
			PreparedStatement ps= con.prepareStatement(selectQuery);
			java.sql.Timestamp ftime = 	new java.sql.Timestamp(filterTime);
			
		    ps.setTimestamp(1, ftime);
		    
			rs = ps.executeQuery();
		    trxs = helper.parseGw(rs);
			
		  } catch (SQLException e) {
			  logger.error(e.getMessage());
		  } 
 	      con.close();
 	      return trxs;
	}

}