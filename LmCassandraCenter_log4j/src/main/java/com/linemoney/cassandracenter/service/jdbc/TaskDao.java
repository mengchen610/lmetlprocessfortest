package com.linemoney.cassandracenter.service.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONException;

import com.linemoney.cassandracenter.helper.ConvertHelper;
import com.linemoney.cassandracenter.helper.LmCassandraCenterConstants;
import com.linemoney.cassandracenter.model.Task;

public class TaskDao extends GenericDAO<Task> {

	private ConvertHelper helper;
	private static final Logger logger = Logger.getLogger(TaskDao.class);

	public TaskDao(ConnectionFactory pool, String tableName) {
		super(pool, tableName);
		helper = new ConvertHelper();
	}

	public List<Task> selectAll() throws SQLException {
		Connection con = pool.getConnection();
		List<Task> tasks = new ArrayList<Task>();
		ResultSet rs = null;
		String selectQuery = "select * from " + tableName
				+ " where taskId like '"
 	    		+ LmCassandraCenterConstants.SERVERCOUNTRY+"%'";
		try {
			PreparedStatement ps = con.prepareStatement(selectQuery);

			rs = ps.executeQuery();
			tasks = helper.parseTask(rs);
		} catch (SQLException e) {
			logger.error(e.getMessage());
		} catch (JSONException e) {
			logger.error(e.getMessage());
		}
        con.close();
		return tasks;
	}

	public List<Task> selectUpdateAll(long filterTime) throws SQLException {
		Connection con = pool.getConnection();
		List<Task> tasks = new ArrayList<Task>();
		ResultSet rs = null;
		String query = "select * from " + tableName + " where updateTime >= ? "
						+ "and taskId like '"
		 	    		+ LmCassandraCenterConstants.SERVERCOUNTRY+"%'";
		try {
			PreparedStatement ps = con.prepareStatement(query);

			java.sql.Timestamp ftime = new java.sql.Timestamp(filterTime);
			ps.setTimestamp(1, ftime);

			rs = ps.executeQuery();
			tasks = helper.parseTask(rs);
		} catch (SQLException e) {
			logger.error(e.getMessage());
		} catch (JSONException e) {
			logger.error(e.getMessage());
		}
		con.close();
		return tasks;
	}

}
