package com.linemoney.cassandracenter.service.services;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.linemoney.cassandracenter.model.Account;
import com.linemoney.cassandracenter.model.ClearingView;
import com.linemoney.cassandracenter.model.GwTransaction;
import com.linemoney.cassandracenter.model.Job;
import com.linemoney.cassandracenter.model.Order;
import com.linemoney.cassandracenter.model.Task;
import com.linemoney.cassandracenter.model.Transaction;
import com.linemoney.cassandracenter.model.User;
import com.linemoney.cassandracenter.service.client.SimpleClient;
import com.linemoney.cassandracenter.service.jdbc.AccountDao;
import com.linemoney.cassandracenter.service.jdbc.CassandraDAO;
import com.linemoney.cassandracenter.service.jdbc.ConnectionFactory;
import com.linemoney.cassandracenter.service.jdbc.GwTransactionDAO;
import com.linemoney.cassandracenter.service.jdbc.JobDAO;
import com.linemoney.cassandracenter.service.jdbc.OrderDao;
import com.linemoney.cassandracenter.service.jdbc.TaskDao;
import com.linemoney.cassandracenter.service.jdbc.TransactionDao;
import com.linemoney.cassandracenter.service.jdbc.UserDAO;
public class CassandraLoadService{
	
	  
	   private SimpleClient client;
	   private ConnectionFactory connectionPool;
	   
	   private static final Logger logger = Logger.getLogger(CassandraLoadService.class);
	   
	   public CassandraLoadService(){
		      this.connectionPool = ConnectionFactory.getInstance();
		      this.client = SimpleClient.getInstance();
	   }
	   
	   public void templateLoad() throws SQLException{
		   if (client == null){
			   System.out.println("error");
		   }
		       client.connect();
		      client.prepareStatement();
		   GwTransactionDAO gwTransactionDAO = new GwTransactionDAO(connectionPool, "linemoney_portal.lm_paymentmanager_transaction");
		   List<GwTransaction> gw_trx = gwTransactionDAO.selectAll();
		     logger.info("Fetch gwTransaction");
		     for (GwTransaction gw : gw_trx){
		    	  client.loadGwData(gw);
		     }
		     logger.info("Fetch gw End, catch :" + gw_trx.size());
		     client.close();
	   }
	      
	   public void loadAllData() throws SQLException{
		      client.connect();
		      client.prepareStatement();
		      OrderDao orderDao = new OrderDao(connectionPool, "linemoney_portal.lm_order");
		      TaskDao taskDao = new TaskDao(connectionPool, "linemoney_portal.lm_task");
		      TransactionDao transactionDao = new TransactionDao(connectionPool, "linemoney_portal.lm_transaction");
		      UserDAO userDao = new UserDAO(connectionPool, "linemoney_portal.User_");
		      JobDAO jobDao = new JobDAO(connectionPool, "linemoney_portal.lm_job");
		      AccountDao accountDao = new AccountDao(connectionPool, "linemoney_portal.lm_account");
		      CassandraDAO cassandraDao = new CassandraDAO(connectionPool, "linemoney_portal.lm_cassandra_view");
		      GwTransactionDAO gwTransactionDAO = new GwTransactionDAO(connectionPool, "linemoney_portal.lm_paymentmanager_transaction");	 
			 List<Order> pushOrder = orderDao.selectAll();
			 for (Order order : pushOrder){
				   client.loadOrderData(order);
				   client.loadOrderStatusData(order);
			 }
			 List<Job> jobs = jobDao.selectAll();
			 for (Job job : jobs){
				  client.loadjobData(job);
			 }
			 List<Task> tasks = taskDao.selectAll();
			 for (Task task: tasks){
				  client.loadTaskData(task);
			 }
			 List<Transaction> transactions = transactionDao.selectAll();
			 for (Transaction transaction : transactions){
				  client.loadTransactionData(transaction);
			 }
			 List<User> users = userDao.selectAll();
		     for (User user : users){
		    	  client.loadUserData(user);
		     }
		     List<Account> accounts = accountDao.selectAll();
		     for (Account account: accounts){
		    	  client.loadAccountData(account);
		     }
		     List<ClearingView> views = cassandraDao.selectAll();
		     for (ClearingView view : views){
		    	 client.loadViewData(view);
		     }
		     List<GwTransaction> gw_trx = gwTransactionDAO.selectAll();
		     for (GwTransaction gw : gw_trx){
		    	  client.loadGwData(gw);
		     }
		     client.close();
	   }
	   
	   public void loadDataAfterUpdate(long filterTime) throws SQLException{
		   client.connect();
		   client.prepareStatement();
		   OrderDao orderDao = new OrderDao(connectionPool, "linemoney_portal.lm_order");
		   TaskDao taskDao = new TaskDao(connectionPool, "linemoney_portal.lm_task");
		   TransactionDao transactionDao = new TransactionDao(connectionPool, "linemoney_portal.lm_transaction");
		      UserDAO userDao = new UserDAO(connectionPool, "linemoney_portal.User_");
		      JobDAO jobDao = new JobDAO(connectionPool, "linemoney_portal.lm_job");
		      AccountDao accountDao = new AccountDao(connectionPool, "linemoney_portal.lm_account");
		      CassandraDAO cassandraDao = new CassandraDAO(connectionPool, "linemoney_portal.lm_cassandra_view");
		      GwTransactionDAO gwTransactionDAO = new GwTransactionDAO(connectionPool, "linemoney_portal.lm_paymentmanager_transaction");	
		    
			 List<Order> pushOrder = orderDao.selectUpdateAll(filterTime);
			 logger.info("Fetch Order Start");
			 for (Order order : pushOrder){
				   client.loadOrderData(order);
				   client.loadOrderStatusData(order);
			 }
			 logger.info("Fetch Order End, catch:" + pushOrder.size());
			 
			 List<Job> jobs = jobDao.selectUpdateAll(filterTime);
			 logger.info("Fetch Job Start");
			 for (Job job : jobs){
				  client.loadjobData(job);
			 }
			 logger.info("Fetch Job End, catch:" + jobs.size());
			 
			 List<Task> tasks = taskDao.selectUpdateAll(filterTime);
			 logger.info("Fetch Task Start");
			 for (Task task: tasks){
				  client.loadTaskData(task);
			 }
			 logger.info("Fetch Task End, catch:" + tasks.size());
			 
			 List<Transaction> transactions = transactionDao.selectUpdateAll(filterTime);
			 logger.info("Fetch Transactions Start");
			 for (Transaction transaction : transactions){
				  client.loadTransactionData(transaction);
			 }
			 logger.info("Fetch Transactions End, catch:" + transactions.size());
			 
			 List<User> users = userDao.selectUpdateAll(filterTime);
			 logger.info("Fetch User Start");
		     for (User user : users){
		    	  client.loadUserData(user);
		     }
		     logger.info("Fetch User End, catch:" + users.size());
		     
		     List<Account> accounts = accountDao.selectUpdateAll(filterTime);
		     logger.info("Fetch User Start");
		     for (Account account: accounts){
		    	  client.loadAccountData(account);
		     }
		     logger.info("Fetch User End, catch:" + accounts.size());
		     
		     List<ClearingView> views = cassandraDao.selectUpdateAll(filterTime);
		     logger.info("Fetch view");
		     for (ClearingView view : views){
		    	 client.loadViewData(view);
		     }
		     logger.info("Fetch view End, catch :" + views.size());
		     
		     
		     List<GwTransaction> gw_trx = gwTransactionDAO.selectUpdateAll(filterTime);
		     logger.info("Fetch gwTransaction");
		     for (GwTransaction gw : gw_trx){
		    	  client.loadGwData(gw);
		     }
		     logger.info("Fetch gw End, catch :" + gw_trx.size());
		     client.close();
	   }
	   
	   
}
