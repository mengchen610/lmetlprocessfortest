package com.linemoney.cassandracenter.service.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONException;

import com.linemoney.cassandracenter.helper.ConvertHelper;
import com.linemoney.cassandracenter.helper.LmCassandraCenterConstants;
import com.linemoney.cassandracenter.model.Job;
import com.linemoney.cassandracenter.model.Order;

public class JobDAO extends GenericDAO<Job> {
	
	private ConvertHelper helper;
	
	private static final Logger logger = Logger.getLogger(JobDAO.class);

	public JobDAO(ConnectionFactory pool, String tableName) {
		super(pool, tableName);
		helper = new ConvertHelper();
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Job> selectAll() throws SQLException {
		 Connection con = pool.getConnection();
		 List<Job> jobs = new ArrayList<Job>();
		 ResultSet rs = null;
  	     String selectQuery = "select * from " + tableName
  	    		+ " as t1 where t1.jobId like '"
 	    		+ LmCassandraCenterConstants.SERVERCOUNTRY+"%'";
  	     try {
			PreparedStatement ps= con.prepareStatement(selectQuery);
			
			rs = ps.executeQuery();
			jobs = helper.parseJob(rs);
			
			for (Job job : jobs){
			     List<String> order_list = new ArrayList<String>();
				 String jobId = job.getJobId();
				 
				 String subquery = "select t2.* , t3.uuid_ as userId_uuid from lm_joborders as t1 left join lm_order as t2 on t1.orderId = t2.orderId left join User_ as t3 on t2.userId = t3.userId where t1.jobId = ?";
				 PreparedStatement subPs = con.prepareStatement(subquery);
				 subPs.setString(1, jobId);
				 ResultSet orderRs = subPs.executeQuery();
				 List<Order> orders = helper.parseOrder(orderRs);
				 for (Order order : orders){
					 
					  String order_String = order.getObj();
					  order_list.add(order_String);
				 }
				 job.setOrder_list(order_list);
			}
		  } catch (SQLException e) {
				logger.error(e.getMessage());
		  } catch (JSONException e) {
			  logger.error(e.getMessage());
		}
  	      con.close();
  	      return jobs;
	}

	@Override
	public List<Job> selectUpdateAll(long filterTime) throws SQLException {
		Connection con = pool.getConnection();
		List<Job> jobs = new ArrayList<Job>();
		 ResultSet rs = null;
 	     String selectQuery = "select * from " + tableName + " where updateTime >= ? "
 	    		+ "and jobId like '"
 	    		+ LmCassandraCenterConstants.SERVERCOUNTRY+"%'";
 	     try {
			PreparedStatement ps= con.prepareStatement(selectQuery);
			java.sql.Timestamp ftime = 	new java.sql.Timestamp(filterTime);
			
		    ps.setTimestamp(1, ftime);
		    
			rs = ps.executeQuery();
			jobs = helper.parseJob(rs);
			
			for (Job job : jobs){
			     List<String> order_list = new ArrayList<String>();
				 String jobId = job.getJobId();
				 String subquery = "select t2.* , t3.uuid_ as userId_uuid from lm_joborders as t1 left join lm_order as t2 on t1.orderId = t2.orderId left join User_ as t3 on t2.userId = t3.userId where t1.jobId = ?";
				 PreparedStatement subPs = con.prepareStatement(subquery);
				 subPs.setString(1, jobId);
				 ResultSet orderRs = subPs.executeQuery();
				 List<Order> orders = helper.parseOrder(orderRs);
				 for (Order order : orders){
					 
					  String order_String = order.getObj();
					  order_list.add(order_String);
				 }
				 job.setOrder_list(order_list);
			}
		  } catch (SQLException e) {
			  logger.error(e.getMessage());
		  } catch (JSONException e) {
			  logger.error(e.getMessage());
		}
 	      con.close();
 	      return jobs;
	}

	
     
}
