package com.linemoney.cassandracenter.service.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONException;

import com.linemoney.cassandracenter.helper.ConvertHelper;
import com.linemoney.cassandracenter.helper.LmCassandraCenterConstants;
import com.linemoney.cassandracenter.model.Order;


public class OrderDao extends GenericDAO<Order>{
	
	private ConvertHelper helper;
	
	private static final Logger logger = Logger.getLogger(OrderDao.class);
	
	public OrderDao(ConnectionFactory pool, String tableName) {
			super(pool, tableName);
			helper =  new ConvertHelper();
	}

	
	public List<Order> selectAll() throws SQLException {
		 System.out.println("test select here");
		 Connection con = pool.getConnection();
		 List<Order> orders = new ArrayList<Order>();
		 ResultSet rs = null;
  	     String selectQuery = "select t1.*  , t2.uuid_ as userId_uuid from "+tableName 
  	    		 +" as t1 inner join User_ t2 on t1.userId = t2.userId where t1.orderId like '"
  	    		 +LmCassandraCenterConstants.SERVERCOUNTRY+"%'";
  	     try {
			PreparedStatement ps= con.prepareStatement(selectQuery);
			
			rs = ps.executeQuery();
			orders = helper.parseOrder(rs);
		  } catch (SQLException e) {
				logger.error(e.getMessage());
		  } catch (JSONException e) {
			  logger.error(e.getMessage());
		}
  	      con.close();
  	      return orders;	
	}

	
	public List<Order> selectUpdateAll(long filterTime) throws SQLException {
		Connection con = pool.getConnection();
		List<Order> orders = new ArrayList<Order>();
		ResultSet rs = null;
		String query = "select t1.*  , t2.uuid_ as userId_uuid from "+tableName 
				+" t1 inner join User_ t2 on t1.userId = t2.userId where t1.updateTime >= ?"
				+" and t1.orderId like '"
 	    		+LmCassandraCenterConstants.SERVERCOUNTRY+"%'";
		try {
			
			PreparedStatement ps= con.prepareStatement(query);
			java.sql.Timestamp ftime = 	new java.sql.Timestamp(filterTime);
			
		    ps.setTimestamp(1, ftime);
			
			rs = ps.executeQuery();
			orders = helper.parseOrder(rs);
		  } catch (SQLException e) {
			  logger.error(e.getMessage());
		  } catch (JSONException e) {
			  logger.error(e.getMessage());
		}
		con.close();
		 return orders;
	}
    
     
}
