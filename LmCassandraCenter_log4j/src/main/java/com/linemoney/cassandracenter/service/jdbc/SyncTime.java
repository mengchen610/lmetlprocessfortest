package com.linemoney.cassandracenter.service.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.apache.log4j.Logger;

public class SyncTime {
	private static final Logger logger = Logger.getLogger(SyncTime.class);
	
	private final String tableName = "linemoney_portal.cassandra_synctime";
    private ConnectionFactory pool;
    
    public SyncTime(){
    	   pool = ConnectionFactory.getInstance();
    	   
    }
    
    public long getLastSyncTime(){
    	Connection con = pool.getConnection();
		ResultSet rs = null;
		String query = "select * from " + tableName + " order by syncTime desc limit 1;";
		PreparedStatement ps;
		long time = 0l;
		try {
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()){
				Timestamp syncTime = rs.getTimestamp("syncTime");	 
				time = syncTime.getTime();
			}
			con.close();
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
		
		return time;
    }
    
    public void insertTime(long time){
    	Connection con = pool.getConnection();
		String query = "insert into " + tableName + " (syncTime) values( ?);";
		PreparedStatement ps;
		try {
			ps = con.prepareStatement(query);
			java.sql.Timestamp ftime = new java.sql.Timestamp(time);
			ps.setTimestamp(1, ftime);
			ps.execute();
			con.close();
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
		
    }
    
    
}
