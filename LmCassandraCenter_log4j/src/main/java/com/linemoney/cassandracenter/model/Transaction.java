package com.linemoney.cassandracenter.model;

public class Transaction {
	private String transactionId;
	private String taskId;
	private String statusCd;
	private long addTime;

	public Transaction(String transactionId, String taskId, String statusCd,
			long addTime) {
		super();
		this.transactionId = transactionId;
		this.taskId = taskId;
		this.statusCd = statusCd;
		this.addTime = addTime;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getStatusCd() {
		return statusCd;
	}

	public void setStatusCd(String statusCd) {
		this.statusCd = statusCd;
	}

	public long getAddTime() {
		return addTime;
	}

	public void setAddTime(long addTime) {
		this.addTime = addTime;
	}
}
