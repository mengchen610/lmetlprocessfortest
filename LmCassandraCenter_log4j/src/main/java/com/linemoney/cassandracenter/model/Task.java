package com.linemoney.cassandracenter.model;

public class Task {
	private String taskId;
	private String orderId;
	private String text;

	public Task(String taskId, String orderId, String text) {
		this.taskId = taskId;
		this.orderId = orderId;
		this.text = text;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
