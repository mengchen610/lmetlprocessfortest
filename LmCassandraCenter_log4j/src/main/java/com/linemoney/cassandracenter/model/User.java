package com.linemoney.cassandracenter.model;

public class User {
       private String userId_uuid;
       private String obj;
       private String languageId;
       
       
	
    public User(String userId_uuid, String obj, String languageId) {
		this.userId_uuid = userId_uuid;
		this.obj = obj;
		this.languageId = languageId;
	}
	public String getUserId_uuid() {
		return userId_uuid;
	}
	public void setUserId_uuid(String userId_uuid) {
		this.userId_uuid = userId_uuid;
	}
	public String getObj() {
		return obj;
	}
	public void setObj(String obj) {
		this.obj = obj;
	}
	public String getLanguageId() {
		return languageId;
	}
	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}
       
	   
       
}
