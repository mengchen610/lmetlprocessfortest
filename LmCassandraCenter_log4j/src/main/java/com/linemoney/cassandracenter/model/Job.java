package com.linemoney.cassandracenter.model;

import java.util.ArrayList;
import java.util.List;

public class Job {
       private String jobId;
       private String statusCd;
       private long addTime;
       private long updateTime;
       private List<String> order_list;
	
       public Job(String jobId, String statusCd, long addTime, long updateTime) {
		
		this.jobId = jobId;
		this.statusCd = statusCd;
		this.addTime = addTime;
		this.updateTime = updateTime;
		this.order_list = new ArrayList<String>();
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getStatusCd() {
		return statusCd;
	}

	public void setStatusCd(String statusCd) {
		this.statusCd = statusCd;
	}

	public long getAddTime() {
		return addTime;
	}

	public void setAddTime(long addTime) {
		this.addTime = addTime;
	}

	public long getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(long updateTime) {
		this.updateTime = updateTime;
	}

	public List<String> getOrder_list() {
		return order_list;
	}

	public void setOrder_list(List<String> order_list) {
		this.order_list = order_list;
	}
       
       
}
