package com.linemoney.cassandracenter.model;

public class Order {
       private String orderId;
       private String userId;
       private long orderTime;
       private String obj;
       private long updateTime;
       private String status;
       
		
		public Order(String orderId, String userId, long orderTime, String obj,
			long updateTime, String status) {
		super();
		this.orderId = orderId;
		this.userId = userId;
		this.orderTime = orderTime;
		this.obj = obj;
		this.updateTime = updateTime;
		this.status = status;
	    }
		
		
		public long getUpdateTime() {
			return updateTime;
		}


		public void setUpdateTime(long updateTime) {
			this.updateTime = updateTime;
		}


		public String getStatus() {
			return status;
		}


		public void setStatus(String status) {
			this.status = status;
		}


		public String getOrderId() {
			return orderId;
		}
		public void setOrderId(String orderId) {
			this.orderId = orderId;
		}
		public String getUserId() {
			return userId;
		}
		public void setUserId(String userId) {
			this.userId = userId;
		}
		public long getOrderTime() {
			return orderTime;
		}
		public void setOrderTime(long orderTime) {
			this.orderTime = orderTime;
		}
		public String getObj() {
			return obj;
		}
		public void setObj(String obj) {
			this.obj = obj;
		}
       
       
       
}