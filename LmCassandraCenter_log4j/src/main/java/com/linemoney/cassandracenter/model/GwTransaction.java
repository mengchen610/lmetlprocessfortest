package com.linemoney.cassandracenter.model;

public class GwTransaction {
		private String trxId;
		private String orderId;
	    private String uuid;
	    private long addTime;
	    private long updateTime;
	    private String obj;
		
	    
	    public GwTransaction(String trxId, String orderId, String uuid,
				long addTime, long updateTime,String obj) {
			super();
			this.trxId = trxId;
			this.orderId = orderId;
			this.uuid = uuid;
			this.addTime = addTime;
			this.obj = obj;
			this.updateTime = updateTime;
		}
	    
		public String getTrxId() {
			return trxId;
		}
		public void setTrxId(String trxId) {
			this.trxId = trxId;
		}
		public String getOrderId() {
			return orderId;
		}
		public void setOrderId(String orderId) {
			this.orderId = orderId;
		}
		public String getUuid() {
			return uuid;
		}
		public void setUuid(String uuid) {
			this.uuid = uuid;
		}
		public long getAddTime() {
			return addTime;
		}
		public void setAddTime(long addTime) {
			this.addTime = addTime;
		}
		public String getObj() {
			return obj;
		}
		public void setObj(String obj) {
			this.obj = obj;
		}
		public long getUpdateTime() {
			return updateTime;
		}
		public void setUpdateTime(long updateTime) {
			this.updateTime = updateTime;
		}
    
    
    
}
