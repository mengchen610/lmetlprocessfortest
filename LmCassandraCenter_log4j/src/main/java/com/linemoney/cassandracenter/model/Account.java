package com.linemoney.cassandracenter.model;

public class Account {
	String accountId;
	String userId;
	String holder_obj;
	String bank_obj;
	String accountTypeCd;
	String account_obj;

	public Account(String accountId, String userId, String holder_obj,
			String bank_obj, String accountTypeCd, String account_obj) {
		this.accountId = accountId;
		this.userId = userId;
		this.holder_obj = holder_obj;
		this.bank_obj = bank_obj;
		this.accountTypeCd = accountTypeCd;
		this.account_obj = account_obj;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getHolder_obj() {
		return holder_obj;
	}

	public void setHolder_obj(String holder_obj) {
		this.holder_obj = holder_obj;
	}

	public String getBank_obj() {
		return bank_obj;
	}

	public void setBank_obj(String bank_obj) {
		this.bank_obj = bank_obj;
	}

	public String getAccountTypeCd() {
		return accountTypeCd;
	}

	public void setAccountTypeCd(String accountTypeCd) {
		this.accountTypeCd = accountTypeCd;
	}

	public String getAccount_obj() {
		return account_obj;
	}

	public void setAccount_obj(String account_obj) {
		this.account_obj = account_obj;
	}
}
