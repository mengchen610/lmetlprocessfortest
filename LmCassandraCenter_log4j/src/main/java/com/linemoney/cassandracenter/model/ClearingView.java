package com.linemoney.cassandracenter.model;

public class ClearingView {
	 private String jobId;
     private String orderId;
     private String jobStatusCd;
     private String taskId;
    
	   private int taskIndex;
     private String view_obj;
     private long jobUpdTime;
     private long orderUpdTime;
     private long taskUpdTime;       
	
	 public ClearingView(String jobId, String orderId, String jobStatusCd,
			String taskId, int taskIndex, String view_obj, long jobUpdTime,
			long orderUpdTime, long taskUpdTime) {
		super();
		this.jobId = jobId;
		this.orderId = orderId;
		this.jobStatusCd = jobStatusCd;
		this.taskId = taskId;
		this.taskIndex = taskIndex;
		this.view_obj = view_obj;
		this.jobUpdTime = jobUpdTime;
		this.orderUpdTime = orderUpdTime;
		this.taskUpdTime = taskUpdTime;
	}
	public String getTaskId() {
			return taskId;
		}
		public void setTaskId(String taskId) {
			this.taskId = taskId;
		}
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getJobStatusCd() {
		return jobStatusCd;
	}
	public void setJobStatusCd(String jobStatusCd) {
		this.jobStatusCd = jobStatusCd;
	}
	public int getTaskIndex() {
		return taskIndex;
	}
	public void setTaskIndex(int taskIndex) {
		this.taskIndex = taskIndex;
	}
	public String getView_obj() {
		return view_obj;
	}
	public void setView_obj(String view_obj) {
		this.view_obj = view_obj;
	}
	public long getJobUpdTime() {
		return jobUpdTime;
	}
	public void setJobUpdTime(long jobUpdTime) {
		this.jobUpdTime = jobUpdTime;
	}
	public long getOrderUpdTime() {
		return orderUpdTime;
	}
	public void setOrderUpdTime(long orderUpdTime) {
		this.orderUpdTime = orderUpdTime;
	}
	public long getTaskUpdTime() {
		return taskUpdTime;
	}
	public void setTaskUpdTime(long taskUpdTime) {
		this.taskUpdTime = taskUpdTime;
	}
}
